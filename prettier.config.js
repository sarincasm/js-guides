module.exports = {
	printWidth: 80,
	tabWidth: 2,
	useTabs: true,
	semi: false,
	singleQuote: true,
	trailingComma: 'es5',
	bracketSpacing: false,
	jsxBracketSameLine: false,
	arrowParens: 'avoid',
	requirePragma: false,
}